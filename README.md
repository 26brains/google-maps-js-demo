# README #

Demo adding a Google Map to a list item using a show/hide option.

Uses:

* [Google Maps JS API](https://developers.google.com/maps/documentation/javascript/)
* [Gmap.js](https://hpneo.github.io/gmaps)
* [jQuery](https://jquery.com/)